import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TimeSlot } from '../models/TimeSlot';
import { v4 as uuidv4 } from 'uuid';
import { parseSecondsFromDuration } from '../utils/duration';

type SliceState = {
  currentTimer?: TimeSlot;
  timeSlots: TimeSlot[];
};

const timeSlotSlice = createSlice({
  name: 'timer',
  initialState: {
    timeSlots: []
  } as SliceState,
  reducers: {
    startTimer: {
      reducer: (state, action: PayloadAction<TimeSlot>) => {
        if (state.currentTimer) return;
        state.currentTimer = action.payload;
      },
      prepare: (description: string, duration?: string) => ({
        payload: {
          id: uuidv4(),
          description,
          startDate: duration
            ? Date.now() - (parseSecondsFromDuration(duration) ?? 0) * 1000
            : Date.now(),
          endDate: undefined
        } as TimeSlot
      })
    },
    setCurrentTimerDescription(state, action: PayloadAction<string>) {
      if (!state.currentTimer) return;
      state.currentTimer.description = action.payload;
    },
    setCurrentTimerDuration(state, action: PayloadAction<string>) {
      if (!state.currentTimer) return;
      const durationSec = parseSecondsFromDuration(action.payload);
      if (!durationSec) return;
      state.currentTimer.startDate = Date.now() - durationSec * 1000;
    },
    stopTimer(state) {
      if (!state.currentTimer) return;
      state.currentTimer.endDate = Date.now();
      state.timeSlots.push(state.currentTimer);
      state.currentTimer = undefined;
    },
    removeTimeSlot(state, action: PayloadAction<string>) {
      const index = state.timeSlots.findIndex(
        (timeSlot) => timeSlot.id === action.payload
      );
      state.timeSlots.splice(index, 1);
    },
    setTimeSlotStatus(state, action: PayloadAction<{ end: Date; id: string }>) {
      const index = state.timeSlots.findIndex(
        (timeSlot) => timeSlot.id === action.payload.id
      );
      state.timeSlots[index].endDate = action.payload.end.getTime();
    }
  }
});

export const {
  startTimer,
  stopTimer,
  setCurrentTimerDescription,
  setCurrentTimerDuration,
  removeTimeSlot,
  setTimeSlotStatus
} = timeSlotSlice.actions;
export default timeSlotSlice.reducer;
