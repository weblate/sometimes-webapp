import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import appStyles from './App.module.scss';
import { Menu } from './components/Menu/Menu';
import { LoadingPage } from './pages/LoadingPage';
import { useCurrentTimerInTitleAndFavicon } from './utils/useCurrentTimerInTitleAndFavicon';

const TimersPage = React.lazy(() => import('./pages/Timers/TimersPage'));
const SignInPage = React.lazy(() => import('./pages/SignInPage'));
const NotFoundPage = React.lazy(() => import('./pages/NotFoundPage'));

export default function App() {
  useCurrentTimerInTitleAndFavicon();
  return (
    <BrowserRouter>
      <div className={appStyles.app}>
        <div className={appStyles.menu}>
          <Menu />
        </div>
        <div className={appStyles.page}>
          <React.Suspense fallback={<LoadingPage />}>
            <Routes>
              <Route path="/" element={<TimersPage />} />
              <Route path="/timers" element={<TimersPage />} />
              <Route path="/signin" element={<SignInPage />} />
              <Route path="*" element={<NotFoundPage />} />
            </Routes>
          </React.Suspense>
        </div>
      </div>
    </BrowserRouter>
  );
}
