import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useUpdatableCurrentTime } from '../../utils/useCurrentDate';
import { AppDispatch, RootState } from '../../redux/store';
import {
  startTimer,
  setCurrentTimerDescription,
  setCurrentTimerDuration,
  stopTimer
} from '../../redux/timerSlice';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFolder,
  faPlay,
  faStop,
  faStopwatch,
  faTag
} from '@fortawesome/free-solid-svg-icons';
import { faCalendarAlt } from '@fortawesome/free-regular-svg-icons';
import timersPageStyles from './TimersPage.module.scss';
import { getDurationString } from './../../utils/duration';

export const HeaderTimer = () => {
  const [currentTime, updateCurrentTime] = useUpdatableCurrentTime(750);
  const dispatch = useDispatch<AppDispatch>();

  const currentTimer = useSelector(
    (state: RootState) => state.timer.currentTimer
  );
  const hasRunningTimer = currentTimer !== undefined;

  const [description, setDescription] = useState(
    currentTimer?.description ?? ''
  );
  const [duration, setDuration] = useState('0:00:00');
  const [durationWhenFocused, setDurationWhenFocused] = useState<string>();
  if (currentTimer !== undefined && !durationWhenFocused) {
    // hasRunningTimer
    const durationToDisplay = getDurationString(
      currentTimer.startDate,
      currentTime
    );
    if (durationToDisplay !== duration) setDuration(durationToDisplay);
  }

  const dispatchStartTimer = () => {
    dispatch(startTimer(description, duration));
    updateCurrentTime();
  };

  return (
    <div className={timersPageStyles.headerCurrentTimer}>
      <input
        className={timersPageStyles.timerLabel}
        placeholder="What are you doing?"
        autoFocus={!hasRunningTimer}
        value={description}
        onChange={(e) => setDescription(e.target.value)}
        onKeyPress={(keyboardEvent) => {
          if (keyboardEvent.key === 'Enter' && !hasRunningTimer)
            dispatchStartTimer();
        }}
        onBlur={() => {
          if (hasRunningTimer)
            dispatch(setCurrentTimerDescription(description));
        }}
      />
      <div className={timersPageStyles.timerOptions}>
        <span className={timersPageStyles.timerProject} tabIndex={0}>
          <FontAwesomeIcon icon={faFolder} />
        </span>
        <span className={timersPageStyles.timerTags} tabIndex={0}>
          <FontAwesomeIcon icon={faTag} />
        </span>
        <span className={timersPageStyles.timerDuration}>
          <input
            onFocus={() => setDurationWhenFocused(duration)}
            onChange={(e) => setDuration(e.target.value)}
            onKeyPress={(keyboardEvent) => {
              if (keyboardEvent.key !== 'Enter') return;
              if (!hasRunningTimer) dispatchStartTimer();
              keyboardEvent.currentTarget.blur();
            }}
            onBlur={() => {
              const durationHasChanged = duration !== durationWhenFocused;
              setDurationWhenFocused(undefined);
              if (hasRunningTimer && durationHasChanged) {
                dispatch(setCurrentTimerDuration(duration));
                updateCurrentTime();
              }
            }}
            value={duration}
          ></input>
        </span>
        <span
          className={
            timersPageStyles.timerPlayStopButton +
            ' ' +
            (hasRunningTimer ? timersPageStyles.stop : timersPageStyles.play)
          }
          tabIndex={0}
          onClick={() => {
            if (hasRunningTimer) {
              dispatch(stopTimer());
              setDescription('');
              setDuration('0:00:00');
            } else {
              dispatchStartTimer();
            }
          }}
        >
          <FontAwesomeIcon icon={hasRunningTimer ? faStop : faPlay} />
        </span>
        <div className={timersPageStyles.timerMode}>
          <FontAwesomeIcon
            icon={faStopwatch}
            className={`${timersPageStyles.timerModeTimer} ${timersPageStyles.timerModeSelected}`}
            tabIndex={0}
            fixedWidth
          />
          <FontAwesomeIcon
            icon={faCalendarAlt}
            className={timersPageStyles.timerModeManual}
            tabIndex={0}
            fixedWidth
          />
        </div>
      </div>
    </div>
  );
};
