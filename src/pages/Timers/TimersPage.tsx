import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../redux/store';
import { removeTimeSlot } from '../../redux/timerSlice';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { Page, Header, Content } from '../../components/Page/Page';
import pageStyles from '../../components/Page/Page.module.scss';
import { HeaderTimer } from './HeaderTimer';
import { getDurationString } from '../../utils/duration';

export default function TimersPage() {
  const timeSlots = useSelector((state: RootState) => state.timer.timeSlots)
    .slice()
    .sort((a, b) => b?.startDate - a?.startDate);
  const dispatch = useDispatch<AppDispatch>();

  return (
    <Page>
      <Header>
        <HeaderTimer />
      </Header>
      <Content>
        <div className={pageStyles.contentTitle}>Timers :</div>
        <ul>
          {timeSlots.map((timer) => (
            <li key={timer.id}>
              {`${timer.description} - ${getDurationString(
                timer.startDate,
                timer.endDate ?? 0
              )} - ${new Date(timer.startDate).toLocaleString()} - ${
                timer.endDate ? new Date(timer.endDate).toLocaleString() : ''
              } - `}
              <FontAwesomeIcon
                icon={faTrashAlt}
                onClick={() => {
                  dispatch(removeTimeSlot(timer.id));
                }}
              />
            </li>
          ))}
        </ul>
      </Content>
    </Page>
  );
}
