import { Page, Header } from '../components/Page/Page';

export default function NotFoundPage() {
  return (
    <Page>
      <Header>Page Not Found</Header>
    </Page>
  );
}
