import { Page, Header } from '../components/Page/Page';

export default function SignInPage() {
  return (
    <Page>
      <Header>Sign In</Header>
    </Page>
  );
}
