import { Page, Header } from '../components/Page/Page';

export const LoadingPage = () => (
  <Page>
    <Header>Loading...</Header>
  </Page>
);
