export interface TimeSlot {
  id: string;
  description: string;
  startDate: number;
  endDate?: number;
}
