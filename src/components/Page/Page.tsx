import React from 'react';
import pageStyles from './Page.module.scss';

interface Props {
  children: React.ReactNode;
}

export const Page = ({ children }: Props) => (
  <div className={pageStyles.page}>{children}</div>
);

export const Header = ({ children }: Props) => (
  <div className={pageStyles.header}>{children}</div>
);

export const Content = ({ children }: Props) => (
  <div className={pageStyles.content}>{children}</div>
);
