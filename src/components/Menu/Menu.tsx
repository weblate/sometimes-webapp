import menuStyles from './Menu.module.scss';
import { Link, NavLink } from 'react-router-dom';
import { Clock } from '../Clock/Clock';
import { useDispatch, useSelector } from 'react-redux';
import { useMediaQuery } from 'usehooks-ts';
import { useCurrentDate } from '../../utils/useCurrentDate';
import { AppDispatch, RootState } from '../../redux/store';
import { setIsMenuCollapsed } from '../../redux/settingsSlice';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faClock,
  faFolderOpen,
  faSignInAlt,
  faTags,
  faUsers
} from '@fortawesome/free-solid-svg-icons';

export const Menu = () => {
  const dispatch = useDispatch<AppDispatch>();
  const isMobile = useMediaQuery('(max-width: 767px)');
  const isMenuCollapsed = useSelector((state: RootState) =>
    isMobile
      ? state.settings.isMenuMobileCollapsed
      : state.settings.isMenuDesktopCollapsed
  );
  const currentDateEvery15sec = useCurrentDate(15000);
  const setMenuCollapsed = (isCollapsed: boolean) =>
    dispatch(setIsMenuCollapsed({ isMobile, isCollapsed }));

  const menuClassName = (isCollapsed: boolean) => {
    return isCollapsed
      ? `${menuStyles.menu} ${menuStyles.collapsed}`
      : menuStyles.menu;
  };
  const linkClassName = (link: { isActive: boolean }) => {
    return link.isActive
      ? `${menuStyles.link} ${menuStyles.active}`
      : menuStyles.link;
  };
  return (
    <div className={menuClassName(isMenuCollapsed)}>
      <div className={menuStyles.header}>
        <Link to="/" className={menuStyles.home} tabIndex={-1}>
          <Clock className={menuStyles.clock} value={currentDateEvery15sec} />
          <span className={menuStyles.appName}>SomeTimes</span>
        </Link>
      </div>
      <div className={menuStyles.content}>
        <div className={menuStyles.group}>
          <span className={menuStyles.title}>Track</span>
          <NavLink to="/" className={linkClassName}>
            <FontAwesomeIcon icon={faClock} fixedWidth />
            <span className={menuStyles.label}>Timers</span>
          </NavLink>
        </div>
        <div className={menuStyles.group}>
          <span className={menuStyles.title}>Manage</span>
          <NavLink to="/projects" className={linkClassName}>
            <FontAwesomeIcon icon={faFolderOpen} fixedWidth />
            <span className={menuStyles.label}>Projects</span>
          </NavLink>
          <NavLink to="/teams" className={linkClassName}>
            <FontAwesomeIcon icon={faUsers} fixedWidth />
            <span className={menuStyles.label}>Teams</span>
          </NavLink>
          <NavLink to="/tags" className={linkClassName}>
            <FontAwesomeIcon icon={faTags} fixedWidth />
            <span className={menuStyles.label}>Tags</span>
          </NavLink>
        </div>
        <div className={menuStyles.group}>
          <span className={menuStyles.title}>Workspace</span>
          <NavLink to="/signin" className={linkClassName}>
            <FontAwesomeIcon icon={faSignInAlt} fixedWidth />
            <span className={menuStyles.label}>Sign in</span>
          </NavLink>
        </div>
      </div>
      <div className={menuStyles.footer}>
        <div
          className={menuStyles.toggleCollapse}
          tabIndex={0}
          onKeyPress={(keyboardEvent) => {
            if (keyboardEvent.key === 'Enter')
              setMenuCollapsed(!isMenuCollapsed);
          }}
          onClick={() => {
            setMenuCollapsed(!isMenuCollapsed);
          }}
        >
          <FontAwesomeIcon
            icon={isMenuCollapsed ? faAngleDoubleRight : faAngleDoubleLeft}
            fixedWidth
          />
        </div>
      </div>
    </div>
  );
};
