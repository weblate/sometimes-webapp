export function getDurationString(start: number, end: number) {
  const forceTwoDigits = (number: number) =>
    number < 10 ? '0' + number : number;
  let durationMs = end - start;
  if (durationMs < 0) durationMs = 0;
  const durationSec = Math.floor(durationMs / 1000);
  const secondes = forceTwoDigits(durationSec % 60);
  const durationMin = Math.floor(durationSec / 60);
  const minutes = forceTwoDigits(durationMin % 60);
  const hours = Math.floor(durationMin / 60);
  return `${hours}:${minutes}:${secondes}`;
}

export function parseSecondsFromDuration(duration: string) {
  const durationParts = duration.split(':').map((part) => parseInt(part));
  if (durationParts.length > 3 || !durationParts.every((part) => !isNaN(part)))
    return;
  let seconds = 0;
  let multiplier = 1;
  while (durationParts.length) {
    seconds += (durationParts.pop() ?? 0) * multiplier;
    multiplier *= 60;
  }
  return seconds;
}
