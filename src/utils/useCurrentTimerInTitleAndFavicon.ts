import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../redux/store';
import { getDurationString } from './duration';
import { useCurrentTime } from './useCurrentDate';

export function useCurrentTimerInTitleAndFavicon() {
  const currentTimer = useSelector(
    (state: RootState) => state.timer.currentTimer
  );
  const currentTime = useCurrentTime();
  const [isFaviconRunning, setIsFaviconRunning] = useState(
    currentTimer !== undefined
  );
  useEffect(() => {
    let title: string;
    const hasRunningTimer = currentTimer !== undefined;
    if (hasRunningTimer) {
      const durationToDisplay = getDurationString(
        currentTimer.startDate,
        currentTime
      );
      title = `${durationToDisplay} - ${currentTimer.description} - SomeTimes`;
    } else {
      title = 'SomeTimes';
    }
    if (title !== document.title) document.title = title;
    if (isFaviconRunning !== hasRunningTimer) {
      document
        .querySelectorAll<HTMLLinkElement>(
          "link[rel~='icon'][data-dynamic-href]"
        )
        .forEach((link) => {
          const iconUrl = link.dataset.dynamicHref?.replaceAll(
            '{0}',
            hasRunningTimer ? 'favicon-running' : 'favicon'
          );
          if (iconUrl) link.href = iconUrl;
        });
      setIsFaviconRunning(hasRunningTimer);
    }
  }, [currentTime, currentTimer, isFaviconRunning]);
}
